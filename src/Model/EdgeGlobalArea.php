<?php

namespace IsobarNZ\Edge\Core\Model;

use DNADesign\Elemental\Extensions\ElementalAreasExtension;
use DNADesign\Elemental\Models\ElementalArea;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;

/**
 * Represents a global area. E.g. header / footer
 *
 * @mixin ElementalAreasExtension
 * @mixin Versioned
 * @method ElementalArea ElementalArea()
 */
class EdgeGlobalArea extends DataObject
{
    use EdgeBlockHolderTrait;

    private static $table_name = 'Edge_GlobalComponent';

    private static $has_one = [
        'ElementalArea' => ElementalArea::class,
    ];

    private static $owns = [
        'ElementalArea',
    ];

    private static $cascade_duplicates = [
        'ElementalArea',
    ];

    /**
     * Add default elemental extension
     *
     * @config
     * @var array
     */
    private static $extensions = [
        'elemental' => ElementalAreasExtension::class,
        'versioned' => Versioned::class,
    ];

    protected function getEdgeElementalArea()
    {
        return $this->ElementalArea();
    }
}
