<?php


namespace IsobarNZ\Edge\Core\Model;

use DNADesign\Elemental\Models\BaseElement;
use DNADesign\Elemental\Models\ElementalArea;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Injector\Injector;

trait EdgeBlockHolderTrait
{
    /**
     * Get the default elemental area this page should use
     *
     * @return ElementalArea
     */
    abstract protected function getEdgeElementalArea();

    /**
     * List of blocks to use.
     * Blocks can either be a list of class names, or injector specifications.
     *
     * If left as `false` blocks are disabled.
     * If set to `true` blocks can be managed as normal.
     * If set to an array, the page will be fixed to those blocks specified
     *
     * @var array
     */
    private static $edge_blocks = false;

    /**
     * This must be implemented on all edge pages to auto-construct
     *
     * @config
     * @var array
     */
    private static $default_records = [];

    public function onBeforeWrite()
    {
        // Ensure elemental area is created
        parent::onBeforeWrite();

        // Only run block initialisation if first write
        if (!$this->isInDB()) {
            $this->scaffoldDefaultBlocks();
        }
    }

    /**
     * Populate this page with all default blocks
     */
    protected function scaffoldDefaultBlocks(): void
    {
        // Ensure area is created
        $area = $this->getEdgeElementalArea();
        if (!$area->exists()) {
            return;
        }

        // Skip if elemental area already scaffolded
        if ($area->Elements()->count()) {
            return;
        }

        // Detect if default blocks are specified as an array specification
        $blockSpecs = $this->getBlocksConfiguration();
        if (!is_array($blockSpecs)) {
            return;
        }

        // Build all default blocks from specs
        foreach ($blockSpecs as $spec) {
            /** @var BaseElement $block */
            $block = Injector::inst()->create($spec['class']);
            $block->update($spec['properties']);
            $block->ParentID = $area->ID;
            $block->write();
        }
    }

    /**
     * Find config for this class
     *
     * @return array|bool|null
     */
    protected function getBlocksConfiguration()
    {
        $class = static::class;
        while ($class && $class !== self::class) {
            $blocks = Config::inst()->get($class, 'edge_blocks', Config::UNINHERITED);
            if (isset($blocks)) {
                return $blocks;
            }
            $class = get_parent_class($class);
        }

        return null;
    }
}
