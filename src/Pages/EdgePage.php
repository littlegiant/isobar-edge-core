<?php

namespace IsobarNZ\Edge\Core\Pages;

use DNADesign\Elemental\Extensions\ElementalPageExtension;
use IsobarNZ\Edge\Core\Model\EdgeBlockHolderTrait;
use Page;

/**
 * Base class for all isobar edge pages
 *
 * @mixin ElementalPageExtension
 */
class EdgePage extends Page
{
    use EdgeBlockHolderTrait;

    private static $table_name = 'Edge_Page';

    /**
     * Hide from page types
     *
     * @var string
     */
    private static $hide_ancestor = EdgePage::class;

    /**
     * Add default elemental extension
     *
     * @config
     * @var array
     */
    private static $extensions = [
        'elemental' => ElementalPageExtension::class,
    ];

    /**
     * @inheritDoc
     */
    protected function getEdgeElementalArea()
    {
        return $this->ElementalArea();
    }
}
