<?php

namespace IsobarNZ\Edge\Core\Admin;

use ReflectionException;
use SilverStripe\Admin\LeftAndMain;
use SilverStripe\Control\Controller;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Core\ClassInfo;
use SilverStripe\Forms\CompositeField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\Tab;
use SilverStripe\Forms\TabSet;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\ValidationException;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\Security\PermissionProvider;
use SilverStripe\Security\Security;
use SilverStripe\Versioned\Versioned;

/**
 * Defines the Single Page Administration interface for the CMS
 *
 * @package SinglePageAdmin
 * @author  Stevie Mayhew
 */
class SingleRecordAdmin extends LeftAndMain implements PermissionProvider
{
    /**
     * @var string
     */
    private static $menu_title = 'Single Record Admin';

    /**
     * @var string
     */
    private static $url_rule = '/$Action/$ID/$OtherID';

    /**
     * @var string
     */
    private static $menu_icon_class = 'font-icon-edit-list';

    private static $url_segment = 'singlerecordadmin';

    /**
     * @var array
     */
    private static $allowed_actions = [
        'EditForm',
    ];

    /**
     * A cached reference to the page record
     *
     * @var DataObject
     */
    protected $record = null;

    /**
     * Helper function for getting the single page instance, existing or created
     *
     * @return DataObject|Versioned
     * @throws ValidationException
     */
    protected function findOrMakeRecord()
    {
        if ($this->record) {
            return $this->record;
        }

        $treeClass = static::config()->get('tree_class');

        /** @var DataObject $record */
        $record = DataObject::get($treeClass)->first();
        if (!$record) {
            $record = $treeClass::create();
            $record->write();
        }

        $this->record = $record;
        return $record;
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function providePermissions()
    {
        $perms = [];

        // Add any custom SinglePageAdmin subclasses.
        foreach (ClassInfo::subclassesFor(static::class) as $class) {
            if ($class === self::class) {
                continue;
            }

            $title = _t("{$class}.MENUTITLE", LeftAndMain::menu_title($class));
            $perms["CMS_ACCESS_" . $class] = [
                'name'     => _t(
                    'CMSMain.ACCESS',
                    "Access to '{title}' section",
                    "Item in permission selection identifying the admin section. Example: Access to 'Files & Images'",
                    ['title' => $title]
                ),
                'category' => _t('Permission.CMS_ACCESS_CATEGORY', 'CMS Access'),
            ];
        }

        return $perms;
    }

    public function canView($member = null)
    {
        // Only subclasses can be viewed
        if (self::class === static::class) {
            return false;
        }
        return parent::canView($member);
    }

    /**
     * @param int       $id
     * @param FieldList $fields
     * @return Form|null
     * @throws ValidationException
     */
    public function getEditForm($id = null, $fields = null)
    {
        $page = $this->findOrMakeRecord();
        $fields = $page->getCMSFields();

        // Check record exists
        if (!$page) {
            return $this->EmptyForm();
        }

        // Check if this record is viewable
        if ($page && !$page->canView()) {
            $response = Security::permissionFailure($this);
            $this->setResponse($response);

            return null;
        }

        $negotiator = $this->getResponseNegotiator();
        $form = Form::create(
            $this,
            "EditForm",
            $fields,
            $this->getCMSActions()
        )->setHTMLID('Form_EditForm');
        $form->addExtraClass('cms-edit-form fill-height flexbox-area-grow');
        $form->loadDataFrom($page);
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');
        $form->setValidationResponseCallback(function (ValidationResult $errors) use ($negotiator, $form) {
            $request = $this->getRequest();
            if ($request->isAjax() && $negotiator) {
                $result = $form->forTemplate();

                return $negotiator->respond($request, [
                    'CurrentForm' => function () use ($result) {
                        return $result;
                    },
                ]);
            }

            return null;
        });

        $this->extend('updateEditForm', $form);

        return $form;
    }

    /**
     * @param HTTPRequest $request
     * @return Form
     * @throws ValidationException
     */
    public function EditForm($request = null)
    {
        return $this->getEditForm();
    }

    /**
     * This function is necessary for for some module functionality
     * that relies on the controller having the current page ID implemented
     *
     * @return int
     * @throws ValidationException
     */
    public function currentPageID()
    {
        return $this->findOrMakeRecord()->ID;
    }

    /**
     * @desc Used for preview controls, mainly links which switch between different states of the page.
     * @return bool
     */
    public function getSilverStripeNavigator()
    {
        return false;
    }

    /**
     * @return mixed
     */
    public function getResponseNegotiator()
    {
        $neg = parent::getResponseNegotiator();
        $controller = $this;
        $neg->setCallback('CurrentForm', function () use (&$controller) {
            error_log($controller->renderWith($this->getTemplatesWithSuffix('_EditForm')));
            return $controller->renderWith($this->getTemplatesWithSuffix('_EditForm'));
        });

        return $neg;
    }

    /**
     * @return FieldList
     * @throws ValidationException
     */
    protected function getCMSActions()
    {
        /** @var DataObject|Versioned $record */
        $record = $this->findOrMakeRecord();

        // Get status of page
        $isPublished = $record->isPublished();
        $stagesDiffer = $record->stagesDiffer();

        // Check permissions
        $canPublish = $record->canPublish();
        $canUnpublish = $record->canUnpublish();
        $canEdit = $record->canEdit();

        // Major actions appear as buttons immediately visible as page actions.
        $majorActions = CompositeField::create()->setName('MajorActions');
        $majorActions->setFieldHolderTemplate(get_class($majorActions) . '_holder_buttongroup');

        // "save", supports an alternate state that is still clickable,
        // but notifies the user that the action is not needed.
        $noChangesClasses = 'btn-outline-primary font-icon-tick';
        if ($canEdit && true) {
            $majorActions->push(
                FormAction::create('save', _t(__CLASS__ . '.BUTTONSAVED', 'Saved'))
                    ->addExtraClass($noChangesClasses)
                    ->setAttribute('data-btn-alternate-add', 'btn-primary font-icon-save')
                    ->setAttribute('data-btn-alternate-remove', $noChangesClasses)
                    ->setUseButtonTag(true)
                    ->setAttribute(
                        'data-text-alternate',
                        _t('SilverStripe\\CMS\\Controllers\\CMSMain.SAVEDRAFT', 'Save draft')
                    )
            );
        }

        // "publish", as with "save", it supports an alternate state to show when action is needed.
        if ($canPublish && true) {
            $majorActions->push(
                $publish = FormAction::create('publish', _t(__CLASS__ . '.BUTTONPUBLISHED', 'Published'))
                    ->addExtraClass($noChangesClasses)
                    ->setAttribute('data-btn-alternate-add', 'btn-primary font-icon-rocket')
                    ->setAttribute('data-btn-alternate-remove', $noChangesClasses)
                    ->setUseButtonTag(true)
                    ->setAttribute('data-text-alternate', _t(__CLASS__ . '.BUTTONSAVEPUBLISH', 'Save & publish'))
            );

            // Set up the initial state of the button to reflect the state of the underlying SiteTree object.
            if ($stagesDiffer) {
                $publish->addExtraClass('btn-primary font-icon-rocket');
                $publish->setTitle(_t(__CLASS__ . '.BUTTONSAVEPUBLISH', 'Save & publish'));
                $publish->removeExtraClass($noChangesClasses);
            }
        }

        // Minor options are hidden behind a drop-up and appear as links (although they are still FormActions).
        $rootTabSet = new TabSet('ActionMenus');
        $moreOptions = new Tab(
            'MoreOptions',
            _t(__CLASS__ . '.MoreOptions', 'More options', 'Expands a view for more buttons')
        );
        $moreOptions->addExtraClass('popover-actions-simulate');
        $rootTabSet->push($moreOptions);
        $rootTabSet->addExtraClass('ss-ui-action-tabset action-menus noborder');

        // Rollback
        if (true && $isPublished && $canEdit && $stagesDiffer) {
            $moreOptions->push(
                FormAction::create('rollback', _t(__CLASS__ . '.BUTTONCANCELDRAFT', 'Cancel draft changes'))
                    ->setDescription(_t(
                        'SilverStripe\\CMS\\Model\\SiteTree.BUTTONCANCELDRAFTDESC',
                        'Delete your draft and revert to the currently published page'
                    ))
                    ->addExtraClass('btn-secondary')
            );
        }

        // Unpublish
        if ($isPublished && $canPublish && true && $canUnpublish) {
            $moreOptions->push(
                FormAction::create('unpublish', _t(__CLASS__ . '.BUTTONUNPUBLISH', 'Unpublish'), 'delete')
                    ->setDescription(_t(__CLASS__ . '.BUTTONUNPUBLISHDESC', 'Remove this page from the published site'))
                    ->addExtraClass('btn-secondary')
            );
        }

        $actions = new FieldList([$majorActions, $rootTabSet]);
        $this->extend('updateCMSActions', $actions);

        return $actions;
    }

    /**
     * @param array $data
     * @param Form  $form
     * @return mixed
     * @throws ValidationException
     */
    public function save($data, $form)
    {
        $currentStage = Versioned::get_stage();
        Versioned::set_stage(Versioned::DRAFT);
        $value = $this->doSave($data, $form);
        Versioned::set_stage($currentStage);
        return $value;
    }

    /**
     * @param array $data
     * @param Form  $form
     * @return HTTPResponse
     * @throws ValidationException
     */
    public function publish($data, $form)
    {
        $data['__publish__'] = '1';

        return $this->doSave($data, $form);
    }

    /**
     * Save the page
     *
     * @param array $data
     * @param Form  $form
     * @return HTTPResponse
     * @throws ValidationException
     */
    public function doSave($data, $form)
    {
        $page = $this->findOrMakeRecord();
        $controller = Controller::curr();
        $publish = isset($data['__publish__']);

        // Check publishing permissions
        $doPublish = !empty($publish);
        if ($page && $doPublish && !$page->canPublish()) {
            return Security::permissionFailure($this);
        }

        // save form data into record
        $form->saveInto($page, true);
        $page->write();
        $this->extend('onAfterSave', $page);

        // Set the response message
        // If the 'Save & Publish' button was clicked, also publish the page
        if ($doPublish) {
            $page->publishRecursive();
            $message = _t(
                'SilverStripe\\CMS\\Controllers\\CMSMain.PUBLISHED',
                "Published '{title}' successfully.",
                ['title' => $page->Title]
            );
        } else {
            $message = _t(
                'SilverStripe\\CMS\\Controllers\\CMSMain.SAVED',
                "Saved '{title}' successfully.",
                ['title' => $page->Title]
            );
        }

        $this->getResponse()->addHeader('X-Status', rawurlencode($message));

        return $this->edit($controller->getRequest());
    }

    /**
     * Unpublish the page
     *
     * @return HTTPResponse
     * @throws ValidationException
     */
    public function unpublish()
    {
        $page = $this->findOrMakeRecord();
        $page->doUnpublish();
        return $this->edit(Controller::curr()->getRequest());
    }

    /**
     * @param array $data
     * @param Form  $form
     * @return HTTPResponse
     * @throws ValidationException
     */
    public function rollback($data, $form)
    {
        $page = $this->findOrMakeRecord();

        $page->extend('onBeforeRollback', $page->ID, $page->Version);

        $id = (isset($page->ID)) ? (int)$page->ID : null;
        $version = (isset($page->Version)) ? (int)$page->Version : null;

        /** @var DataObject|Versioned $record */
        $record = Versioned::get_latest_version($this->config()->get('tree_class'), $id);
        if ($record && !$record->canEdit()) {
            return Security::permissionFailure($this);
        }

        if ($version) {
            $record->rollbackRecursive($version);
            $message = _t(
                __CLASS__ . '.ROLLEDBACKVERSIONv2',
                "Rolled back to version #{version}.",
                ['version' => $page->Version]
            );
        } else {
            $record->doRevertToLive();
            $message = _t(
                __CLASS__ . '.ROLLEDBACKPUBv2',
                "Rolled back to published version."
            );
        }

        $this->getResponse()->addHeader('X-Status', rawurlencode($message));

        // Can be used in different contexts: In normal page edit view,
        // in which case the redirect won't have any effect.
        // Or in history view, in which case a revert causes the CMS to re-load the edit view.
        // The X-Pjax header forces a "full" content refresh on redirect.
        $url = Controller::curr()->getRequest();
        $this->getResponse()->addHeader('X-ControllerURL', $url->getURL());
        // @TODO: Redirect to the base url of the form - 24/11/17 Ryan Potter
        $this->getRequest()->addHeader('X-Pjax', 'Content');
        $this->getResponse()->addHeader('X-Pjax', 'Content');

        return $this->getResponseNegotiator()->respond($this->getRequest());
    }


    /**
     * @param HTTPRequest $request
     * @return mixed
     * @throws ValidationException
     */
    public function edit($request)
    {
        $controller = Controller::curr();
        $form = $this->EditForm($request);

        $return = $this->customise([
            'Backlink' => $controller->Link(),
            'EditForm' => $form,
        ])->renderWith($this->getTemplatesWithSuffix('_Content'));

        if ($request->isAjax()) {
            return $return;
        } else {
            return $controller->customise([
                'Content' => $return,
            ]);
        }
    }

    /**
     * @return string
     */
    public function Backlink()
    {
        return $this->Link();
    }

    public function actionComplete($form, $message)
    {
        $this->getResponse()->addHeader('X-Status', rawurlencode($message));
        $controller = Controller::curr();
        return $this->edit($controller->getRequest());
    }

    public function Breadcrumbs($unlinked = false)
    {
        $breadcrumbs = parent::Breadcrumbs($unlinked);
        $this->extend('updateBreadcrumbs', $breadcrumbs);
        return $breadcrumbs;
    }
}
