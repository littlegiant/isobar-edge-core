<?php

namespace IsobarNZ\Edge\Core\Extensions;

use DNADesign\Elemental\Models\BaseElement;
use IsobarNZ\Edge\Core\Security\Permissions;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Security\Permission;

/**
 * Edge customisations to elemenal areas
 *
 * @property BaseElement $owner
 */
class EdgeBaseElementExtension extends DataExtension
{
    public function canCreate($member)
    {
        // Note: This action should only deny behaviour, not permit
        if (!Permission::checkMember($member, Permissions::EDGE_MODIFY_STRUCTURE)) {
            return false;
        }

        return null;
    }

    public function canDelete($member)
    {
        // Don't let authors delete records they can't create
        return $this->canCreate($member);
    }
}
