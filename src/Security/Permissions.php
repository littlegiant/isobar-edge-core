<?php

namespace IsobarNZ\Edge\Core\Security;

use SilverStripe\Security\PermissionProvider;

class Permissions implements PermissionProvider
{
    const EDGE_MODIFY_STRUCTURE = 'EDGE_MODIFY_STRUCTURE';

    public function providePermissions()
    {
        return [
            self::EDGE_MODIFY_STRUCTURE => [
                'name'     => _t(__CLASS__ . '.NAME', 'Modify structure of pages'),
                'category' => _t(__CLASS__ . '.CATEGORY', 'Edge Configuration')
            ]
        ];
    }
}
